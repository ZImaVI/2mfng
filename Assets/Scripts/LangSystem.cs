using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class LangSystem : MonoBehaviour
{
    [Header("Buttons")]
    public Text ButtonPlay;
    public Text ButtonOptions;
    public Text ButtonExit;
    public Text NewGame_Button;
    public Text ContinueButton;
    [Header("Tabs")]
    public Text VideoTab;
    public Text AudioTab;
    [Header("Texts")]
    public Text Master;
    public Text Music;
    public Text Effects;
    public Text Dialogs;
    [Header("Other")]
    private string json;
    public static lang lng = new lang();
    public Dropdown DropDown;

    private void Awake()
    {
        if (!PlayerPrefs.HasKey("Language"))
        {
            if(Application.systemLanguage == SystemLanguage.Russian || Application.systemLanguage == SystemLanguage.Ukrainian || Application.systemLanguage == SystemLanguage.Belarusian)
            {
                PlayerPrefs.SetString("Language", "ru_RU");
            }
            else
            {
                PlayerPrefs.SetString("Language", "en_US");
            }
        }
        LangLoad();
    }
    private void Start()
    {
        List<string> strs = new List<string>() { "English", "�������" };
        DropDown.ClearOptions();
        DropDown.AddOptions(strs);
        if (PlayerPrefs.GetString("Language") == "ru_RU")
        {
            DropDown.value = 1;
        }
        else if (PlayerPrefs.GetString("Language") == "en_US")
        {
            DropDown.value = 0;
        }
    }
    void LangLoad()
    {
        json = File.ReadAllText(Application.streamingAssetsPath + "/Languages/" + PlayerPrefs.GetString("Language") + ".json");
        lng = JsonUtility.FromJson<lang>(json);

        ButtonPlay.text = lng.BtnPlay;
        ButtonOptions.text = lng.BtnOptions;
        ButtonExit.text = lng.BtnExit;
        NewGame_Button.text = lng.BtnNewGame;
        ContinueButton.text = lng.BtnContinue;

        VideoTab.text = lng.TabVideo;
        AudioTab.text = lng.TabAudio;

        Master.text = lng.AudioSettMaster;
        Music.text = lng.AudioSettMusic;
        Effects.text = lng.AudioSettEffects;
        Dialogs.text = lng.AudioSettDialogs;
    }
    public void SetEng()
    {
        PlayerPrefs.SetString("Language", "en_US");
        LangLoad();
    }
    public void SetRus()
    {
        PlayerPrefs.SetString("Language", "ru_RU");
        LangLoad();
    }
    public void SetLang()
    {
        if(DropDown.value == 1)
        {
            PlayerPrefs.SetString("Language", "ru_RU");
            LangLoad();
        }
        else
        {
            PlayerPrefs.SetString("Language", "en_US");
            LangLoad();
        }
    }
}   
public class lang
{
    public string BtnPlay;
    public string BtnOptions;
    public string BtnExit;
    public string BtnNewGame;
    public string BtnContinue;
    public string TabVideo;
    public string TabAudio;
    public string AudioSettMaster;
    public string AudioSettMusic;
    public string AudioSettEffects;
    public string AudioSettDialogs;
}
