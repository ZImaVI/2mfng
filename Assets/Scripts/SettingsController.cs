using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class SettingsController : MonoBehaviour
{
    [Header("Audio Settings")]
    public Slider[] sliders;
    public Text[] texts;
    [Header("Video Settings")]
    public Dropdown DropDown;
    void Start()
    {
        //QualitySettings.names;
        DropDown.ClearOptions();
        DropDown.AddOptions(QualitySettings.names.ToList());
        DropDown.value = QualitySettings.GetQualityLevel();
    }

    void Update()
    {
        int buff = sliders.Length;
        for (int i = 0; i < buff; i++)
        {
            texts[i].text = string.Format("{0:0}%", sliders[i].value * 100);
        }
    }

    public void SetQuality()
    {
        QualitySettings.SetQualityLevel(DropDown.value);
    }

}
