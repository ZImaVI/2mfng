using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashLightController : MonoBehaviour
{
    public AudioSource SwichSound;
    public GameObject FlashLight;
    public KeyCode KeyActivation;
    [HideInInspector]
    public bool isOn;
    void Start()
    {
        isOn = false;
        FlashLight.SetActive(isOn);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyActivation))
        {
            switch (isOn)
            {
                case true:
                    OnOffLight(false);
                    break;
                case false:
                    OnOffLight(true);
                    break;
                default:
                    throw new UnityException("Variable type \"System.boolen\" was \"broken\" cause type \"System.boolen\" contains somthis else than \"true\" or \"false\"");
            }
        }
    }

    public void OnOffLight(bool Swich)
    {
        if (Swich)
        {
            isOn = true;
            FlashLight.SetActive(isOn);
            SwichSound.PlayOneShot(SwichSound.clip);
        }
        else
        {
            isOn = false;
            FlashLight.SetActive(isOn);
            SwichSound.PlayOneShot(SwichSound.clip);
        }
    }
}
