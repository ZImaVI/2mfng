using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightBracker : MonoBehaviour
{
    public GameObject[] Light;
    public float MinTimeSeconds;
    public float MaxTimeSeconds;
    public bool IsOn;
    public bool WhileTrue = true;

    void Start()
    {
        StartCoroutine(RandomOnOff());
    }
    IEnumerator RandomOnOff()
    {
        do
        {
            if (IsOn)
            {
                foreach (var item in Light)
                {
                    item.SetActive(false);
                    yield return new WaitForSeconds(Random.Range(MinTimeSeconds, MaxTimeSeconds));
                }
                IsOn = false;
            }
            else
            {
                foreach (var item in Light)
                {
                    item.SetActive(true);
                    yield return new WaitForSeconds(Random.Range(MinTimeSeconds, MaxTimeSeconds));
                }
                IsOn = true;
            }
        } while (WhileTrue == true);
        yield return null;
    }
}
