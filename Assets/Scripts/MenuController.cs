using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    public int SceneID;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnBtnExit()
    {
        Application.Quit();
    }

    public void OnNewGameBtn()
    {
        SceneManager.LoadScene(SceneID);
    }
}
