using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutTriggerController : MonoBehaviour
{
    public Vector3 Spawner;
    public bool InTrigger = false;
    private void OnTriggerEnter(Collider other)
    {
        InTrigger = true;
        if (other.tag == "Player")
        {
            other.transform.position = Spawner;
        }
    }
    private void OnTriggerStay(Collider other)
    {
        do
        {
            if (other.tag == "Player")
            {
                other.transform.position = Spawner;
                if (other.transform.position == new Vector3(other.transform.position.x, -39f, other.transform.position.z))
                {
                    InTrigger = false;
                }
            }
        } while (InTrigger == true);
    }
    private void OnTriggerExit()
    {
        
    }
}
