using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class ResursesLoader
{
    string Directory;
    public ResursesLoader()
    {
        Directory = Path.Combine(Application.streamingAssetsPath, "Resurses");
    }

    public Sprite LoadSprite(string imageName)
    {
        var path = Path.Combine(Directory, imageName);

        byte[] bytes = null;
        Sprite sprite = null;

        var file = new FileInfo(path);

        if (file.Exists)
        {
            bytes = File.ReadAllBytes(path);
            var texture = new Texture2D(512, 512, TextureFormat.DXT1, false, true);
            texture.LoadImage(bytes);
            sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
        }
        return sprite;
    }
}
