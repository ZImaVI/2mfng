using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioController : MonoBehaviour
{
    public AudioMixerGroup Mixer;
    public string MixerGroup;

    public void ToogleMusic(bool enabled)
    {
        if (enabled)
        {
            Mixer.audioMixer.SetFloat(MixerGroup, 0);
        }
        else
        {
            Mixer.audioMixer.SetFloat(MixerGroup, -80);
        }
    }
    public void ChangeVol(float vol)
    {
        Mixer.audioMixer.SetFloat(MixerGroup, Mathf.Lerp(-80, 0, vol));
    }
}
