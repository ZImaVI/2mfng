using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScene : MonoBehaviour
{
    [Header("Scene ID")]
    public int id;
    [Header("Other Objects")]
    public Image image;
    public Text text;
    public bool IsLoadSprite;
    // Start is called before the first frame update
    void Start()
    {
        if (IsLoadSprite)
        {
            StartCoroutine(AsyncLoading());
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator AsyncLoading()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(id);
        while (!operation.isDone)
        {
            float progress = operation.progress / 0.9f;
            image.fillAmount = progress;
            text.text = string.Format("{0:0}%", progress * 100);
            yield return null;
        }
    }
    public void ToLoading()
    {
        SceneManager.LoadScene(id);
    }
}
